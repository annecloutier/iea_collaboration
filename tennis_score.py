# Python - Your task is to implement a tennis scoring program.
# Summary of tennis scoring:
 
# A game is won by the first player to have won at least four points in total and at least two points more than the opponent.
# The running score of each game is described in a manner particular to tennis: 
#     scores from zero to three points are described as "love", "fifteen", "thirty", and "forty" respectively.
# If at least three points have been scored by each player, and the scores are equal, the score is "deuce".
# If at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is "advantage" for the player in the lead.

# start making 2 players
# track scores for each players
# to win you have to have at least 4 points AND at least 2 points more than opponent 
import random   

def enter_score(score, player):
    
    return score, player
    
player1 = input("Enter player name: ")
player2 = input("Enter player name: ")
score_0 = "love"
score_1 = 15
score_2 = 30
score_3 = 40
# if player1 total = player2 total == "deuce"

# add random to randomize who will score 
players = [player1, player2]

player1_score = 0
player2_score = 0

count = 0  # using count temporarily to prevent an infinite loop
while count < 5:
    set_winner = random.choice(players)
    print(set_winner)
    # now that we have a random winner, we need to add points to the set_winner's score.  
    count += 1